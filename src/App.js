import './App.css';
import Menu from './components/Menu';
import Sidebar from './components/Sidebar';


function App() {
  return (
    <div className="App">
      <Menu />
     <Sidebar />
    </div>
  );
}

export default App;
