import React from "react";

function Sidebar() {
    return (
        <div>
            <div className="text-center" style={{ "width": "250px", "height": "1000px", "borderRight": "5px solid grey" }}>
                <div style={{ "paddingTop": "20px","fontWeight":"bold"}}>
                    OTHER TOPICS
                    <hr style={{"border":"solid 2px purple","width":"180px"}}/>
                </div>
                <div>
                    <a>Hibernate CRUD Operations</a><br /><br />
                    <a>OneToOne Hibernate Mapping Unidirectional</a><br /><br />
                    <a>OneToOne Hibernate Mapping Bidirectional</a><br /><br />
            </div>
            </div>
        </div>
    );
}

export default Sidebar;